package dlemontagner.moneytransfer.domain;
/*
As recommended in Spring documentation, this is a special no-op
marker interface for @ComponentScan purpose.
*/
public interface MoneyTransferDomain {
}
