package dlemontagner.moneytransfer.domain.entities;

import dlemontagner.moneytransfer.domain.exceptions.InsufficientBalanceException;

import java.math.BigDecimal;
import java.util.UUID;

public record Account(UUID id, BigDecimal balance) {
    private static final BigDecimal BALANCE_MIN_VALUE = BigDecimal.ZERO;

    public Account(BigDecimal initialBalance) {
        this(UUID.randomUUID(),initialBalance);
    }

    public Account withdraw(BigDecimal amount) {
        if (balance.subtract(amount).compareTo(BALANCE_MIN_VALUE) < 0) {
            throw new InsufficientBalanceException("Insufficient balance for withdrawal.");
        }
        return new Account(id, balance.subtract(amount));
    }

    public Account deposit(BigDecimal amount) {
        BigDecimal newBalance = balance.add(amount);
        return new Account(id, newBalance);
    }
}
