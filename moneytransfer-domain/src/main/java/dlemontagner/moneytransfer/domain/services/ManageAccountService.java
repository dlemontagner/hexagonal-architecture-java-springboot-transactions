package dlemontagner.moneytransfer.domain.services;

import ddd.DomainService;
import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.ports.api.ManageAccountUseCase;
import dlemontagner.moneytransfer.domain.ports.spi.AccountsRepository;

import java.math.BigDecimal;
import java.util.UUID;

@DomainService
public class ManageAccountService implements ManageAccountUseCase {
    private AccountsRepository accountsRepository;
    public ManageAccountService(AccountsRepository accountsRepository){
        this.accountsRepository = accountsRepository;
    }

    public Account openAccount(BigDecimal initialBalance){
        Account newAccount = new Account(initialBalance);
        return accountsRepository.save(newAccount);
    }
    public Account viewAccount(UUID id) {
        return accountsRepository.getById(id);
    }

    public void transferMoney(UUID sourceId, UUID destinationId, BigDecimal amount) {
        Account source = this.viewAccount(sourceId);
        Account destination = this.viewAccount(destinationId);
        this.transferMoney(source, destination, amount);
    }
    public void transferMoney(Account source, Account destination, BigDecimal amount) {
        source = source.withdraw(amount);
        destination = destination.deposit(amount);

        accountsRepository.save(source);
        accountsRepository.save(destination);
    }
}

