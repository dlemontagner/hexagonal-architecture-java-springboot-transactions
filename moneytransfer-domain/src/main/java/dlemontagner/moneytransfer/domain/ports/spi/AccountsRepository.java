package dlemontagner.moneytransfer.domain.ports.spi;

import dlemontagner.moneytransfer.domain.entities.Account;

import java.util.UUID;

public interface AccountsRepository {
    public Account getById(UUID id);
    public Account save(Account account);
}
