package dlemontagner.moneytransfer.domain.ports.spi.stubs;

import ddd.Stub;
import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.ports.spi.AccountsRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Stub
public class InMemoryAccountsRepository implements AccountsRepository {
    private final Map<UUID, Account> accounts = new HashMap<>();

    @Override
    public Account getById(UUID id) {
        return accounts.get(id);
    }

    @Override
    public Account save(Account account) {
        accounts.put(account.id(), account);
        return account;
    }
}
