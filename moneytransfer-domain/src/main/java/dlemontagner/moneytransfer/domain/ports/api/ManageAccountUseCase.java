package dlemontagner.moneytransfer.domain.ports.api;

import dlemontagner.moneytransfer.domain.entities.Account;

import java.math.BigDecimal;
import java.util.UUID;

public interface ManageAccountUseCase {
    public Account openAccount(BigDecimal initialBalance);
    public Account viewAccount(UUID id);
    public void transferMoney(UUID sourceId, UUID destinationId, BigDecimal amount);
    public void transferMoney(Account source, Account destination, BigDecimal amount);
}
