package dlemontagner.moneytransfer.domain;

import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.exceptions.InsufficientBalanceException;
import dlemontagner.moneytransfer.domain.parameterResolvers.AccountServiceParameterResolver;
import dlemontagner.moneytransfer.domain.services.ManageAccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(AccountServiceParameterResolver.class)
class ManageAccountServiceTest {
    @Test
    public void should_create_and_store_account(ManageAccountService manageAccountService){
        Account newAccount = manageAccountService.openAccount(BigDecimal.valueOf(1500));
        assertNotNull(newAccount.id());
        assertEquals(newAccount.balance(),BigDecimal.valueOf(1500));

        Account storedAccount = manageAccountService.viewAccount(newAccount.id());
        assertEquals(newAccount.id(), storedAccount.id());
        assertEquals(newAccount.balance(),storedAccount.balance());
    }

    @Test
    public void should_transfer_money_successfully(ManageAccountService manageAccountService) {
        Account sourceAccount = manageAccountService.openAccount(BigDecimal.valueOf(5000));
        Account destinationAccount = manageAccountService.openAccount(BigDecimal.valueOf(2000));

        manageAccountService.transferMoney(sourceAccount, destinationAccount, BigDecimal.valueOf(1000));

        assertEquals(BigDecimal.valueOf(4000), manageAccountService.viewAccount(sourceAccount.id()).balance());
        assertEquals(BigDecimal.valueOf(3000), manageAccountService.viewAccount(destinationAccount.id()).balance());
    }

    @Test
    public void should_fail_transfer_because_insufficient_balance(ManageAccountService manageAccountService) {
        Account sourceAccount = manageAccountService.openAccount(BigDecimal.valueOf(2000));
        Account destinationAccount = manageAccountService.openAccount(BigDecimal.valueOf(8000));

        assertThrows(InsufficientBalanceException.class,
                () -> manageAccountService.transferMoney(sourceAccount, destinationAccount, BigDecimal.valueOf(4000))
        );
    }

    @Test
    public void should_transfer_money_using_uuids(ManageAccountService manageAccountService){
        Account sourceAccount = manageAccountService.openAccount(BigDecimal.valueOf(5000));
        Account destinationAccount = manageAccountService.openAccount(BigDecimal.valueOf(2000));

        manageAccountService.transferMoney(sourceAccount.id(), destinationAccount.id(), BigDecimal.valueOf(1000));

        assertEquals(BigDecimal.valueOf(4000), manageAccountService.viewAccount(sourceAccount.id()).balance());
        assertEquals(BigDecimal.valueOf(3000), manageAccountService.viewAccount(destinationAccount.id()).balance());
    }
}

