package dlemontagner.moneytransfer.domain.parameterResolvers;

import dlemontagner.moneytransfer.domain.ports.spi.AccountsRepository;
import dlemontagner.moneytransfer.domain.ports.spi.stubs.InMemoryAccountsRepository;
import dlemontagner.moneytransfer.domain.services.ManageAccountService;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class AccountServiceParameterResolver implements ParameterResolver {
    @Override
    public boolean supportsParameter(ParameterContext parameterContext,
                                     ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == ManageAccountService.class;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext,
                                   ExtensionContext extensionContext) throws ParameterResolutionException {
        AccountsRepository accountsRepository = new InMemoryAccountsRepository();
        return new ManageAccountService(accountsRepository);
    }
}