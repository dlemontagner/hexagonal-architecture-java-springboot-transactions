package dlemontagner.moneytransfer.architecture;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchTests;

@AnalyzeClasses(packages = "dlemontagner.moneytransfer", importOptions = { ImportOption.DoNotIncludeTests.class })
public class ArchitectureTest {
    @ArchTest
    public static final ArchTests hexagonalArchTests = ArchTests.in(HexagonalArchitectureRules.class);
}