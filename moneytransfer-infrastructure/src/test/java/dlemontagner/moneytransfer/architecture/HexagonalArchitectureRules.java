package dlemontagner.moneytransfer.architecture;

import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import ddd.DomainService;
import ddd.Stub;

import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAPackage;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAnyPackage;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

public class HexagonalArchitectureRules {
    @ArchTest
    static final ArchRule domainShouldHaveNoDependencies =
            noClasses()
                    .that()
                    .resideInAPackage("..domain..")
                    .should()
                    .dependOnClassesThat()
                    .resideOutsideOfPackages("..domain..", "..ddd..", "..java..");

    @ArchTest
    static final ArchRule domainServicesShouldBeInServicesPackage =
            noClasses()
                    .that()
                    .resideOutsideOfPackage("..domain.services..")
                    .should()
                    .beAnnotatedWith(DomainService.class);

    @ArchTest
    static final ArchRule domainServicesShouldImplementApiInterfaces =
            classes()
                    .that()
                    .areAnnotatedWith(DomainService.class)
                    .should()
                    .implement(resideInAPackage("..domain.ports.api.."));

    @ArchTest
    static final ArchRule noClassesShouldDependOnDomainServices =
            noClasses()
                    .should()
                    .dependOnClassesThat(resideInAPackage("..domain.services.."))
                    .as("No classes should depend on domain services")
                    .because("Domain API ports should be used instead");

    @ArchTest
    static final ArchRule domainPortsShouldBeInterfacesExceptForStubs =
            classes()
                    .that()
                    .resideInAPackage("..domain.ports..")
                    .and()
                    .resideOutsideOfPackage("..domain.ports.spi.stubs..")
                    .should()
                    .beInterfaces();

    @ArchTest
    static final ArchRule domainStubsShouldBeInSpiStubPackage =
            noClasses()
                    .that()
                    .resideOutsideOfPackage("..domain.ports.spi.stubs..")
                    .should()
                    .beAnnotatedWith(Stub.class);
    @ArchTest
    static final ArchRule apiAndSpiPortsShouldNotDependOnEachOthersExceptForStubs =
            noClasses()
                    .that()
                    .resideInAPackage("..domain.ports..")
                    .and()
                    .resideOutsideOfPackage("..domain.ports.spi.stubs..")
                    .should()
                    .dependOnClassesThat()
                    .resideInAnyPackage("..domain.ports.api..", "..domain.ports.spi..");

    @ArchTest
    static final ArchRule adaptersShouldNotDependOnOneAnother =
            slices()
                    .matching("..infrastructure.(adapters).(*)..").namingSlices("$1 '$2'")
                    .should().notDependOnEachOther();
    @ArchTest
    static final ArchRule adaptersShouldNotBeCalledDirectly =
            noClasses()
                    .that()
                    .resideOutsideOfPackages("..infrastructure.adapters.spi..", "..infrastructure.adapters.api..")
                    .should()
                    .dependOnClassesThat(resideInAnyPackage("..infrastructure.adapters.spi..", "..infrastructure.adapters.api.."));

    @ArchTest
    static final ArchRule requestsClassesShouldEndWithRequest =
            classes()
                    .that()
                    .resideInAPackage("..infrastructure.requests..")
                    .should()
                    .haveSimpleNameEndingWith("Request")
                    .andShould()
                    .beRecords();

    @ArchTest
    static final ArchRule resourcesClassesShouldEndWithResource =
            classes()
                    .that()
                    .resideInAPackage("..infrastructure.resources..")
                    .should()
                    .haveSimpleNameEndingWith("Resource")
                    .andShould()
                    .beRecords();

}
