package dlemontagner.moneytransfer.infrastructure.adapters.api.controllers;

import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.ports.api.ManageAccountUseCase;
import dlemontagner.moneytransfer.domain.ports.spi.AccountsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
class AccountControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ManageAccountUseCase accountManager;

	@SpyBean
	private AccountsRepository accountsRepository;

	@Test
	public void givenAccount_whenGetAccounts_thenStatus200() throws Exception {
		Account newAccount = accountManager.openAccount(BigDecimal.valueOf(1000));

		mockMvc.perform(get("/accounts/%s".formatted(newAccount.id()))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(newAccount.id().toString()))
				.andExpect(jsonPath("$.balance").value(1000));
	}

	@Test
	public void whenPostAccounts_thenStatus200() throws Exception {
		mockMvc.perform(post("/accounts")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"initialBalance\": \"1250\" }"))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").isNotEmpty())
				.andExpect(jsonPath("$.balance").value(1250));
	}

	@Test
	public void whenPostTransfer_thenStatus200() throws Exception {
		Account sourceAccount = accountManager.openAccount(BigDecimal.valueOf(3000));
		Account destinationAccount = accountManager.openAccount(BigDecimal.valueOf(1000));

		mockMvc.perform(post("/transfer")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{     \"amount\": \"500\"," +
								"    \"source\": \""+sourceAccount.id()+"\"," +
								"    \"destination\": \""+destinationAccount.id()+"\" }"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value("success"))
				.andExpect(jsonPath("$.message").value("Transfer completed successfully."));

		// Assert that transfer has been done
		assertThat(accountManager.viewAccount(sourceAccount.id()).balance())
				.isEqualByComparingTo(BigDecimal.valueOf(2500));

		assertThat(accountManager.viewAccount(destinationAccount.id()).balance())
				.isEqualByComparingTo(BigDecimal.valueOf(1500));
	}

	@Test
	public void whenPostTransfer_andInsufficientBalance_thenStatus400_andProblemDetail() throws Exception {
		Account sourceAccount = accountManager.openAccount(BigDecimal.valueOf(200));
		Account destinationAccount = accountManager.openAccount(BigDecimal.valueOf(1000));

		mockMvc.perform(post("/transfer")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{     \"amount\": \"500\"," +
								"    \"source\": \""+sourceAccount.id()+"\"," +
								"    \"destination\": \""+destinationAccount.id()+"\" }"))
				.andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
				.andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.detail").value("Insufficient balance for withdrawal."));
	}

	@Test
	public void whenPostTransfer_andError_thenStatus500_andProblemDetail_andRollback() throws Exception {
		Account sourceAccount = accountManager.openAccount(BigDecimal.valueOf(2000));
		Account destinationAccount = accountManager.openAccount(BigDecimal.valueOf(1000));

		// Simulate a RuntimeException while saving destination account
		when(accountsRepository.save(
				argThat(new AccountIdArgumentMatcher(destinationAccount))))
				.thenThrow(new RuntimeException());

		// Assert that response is a ProblemDetail
		mockMvc.perform(post("/transfer")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{     \"amount\": \"500\"," +
								"    \"source\": \""+sourceAccount.id()+"\"," +
								"    \"destination\": \""+destinationAccount.id()+"\" }"))
				.andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
				.andExpect(status().is5xxServerError());

		// Assert that transaction has been rolled back
		assertThat(accountManager.viewAccount(sourceAccount.id()).balance())
				.isEqualByComparingTo(sourceAccount.balance());
	}
}
