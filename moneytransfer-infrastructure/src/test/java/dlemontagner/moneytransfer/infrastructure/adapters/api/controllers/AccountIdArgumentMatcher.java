package dlemontagner.moneytransfer.infrastructure.adapters.api.controllers;

import dlemontagner.moneytransfer.domain.entities.Account;
import org.mockito.ArgumentMatcher;

public class AccountIdArgumentMatcher implements ArgumentMatcher<Account> {
    private Account left;

    public AccountIdArgumentMatcher(Account left) {
        this.left = left;
    }

    @Override
    public boolean matches(Account right) {
        return left.id().equals(right.id());
    }

}
