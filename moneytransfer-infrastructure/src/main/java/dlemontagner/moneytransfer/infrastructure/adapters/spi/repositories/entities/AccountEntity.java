package dlemontagner.moneytransfer.infrastructure.adapters.spi.repositories.entities;

import dlemontagner.moneytransfer.domain.entities.Account;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class AccountEntity {

    @Id
    private UUID id;

    @Column(nullable = false)
    private BigDecimal balance;

    public AccountEntity() { }

    public AccountEntity(UUID id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public Account toDomain() {
        return new Account(id,balance);
    }

    public static AccountEntity fromDomain(Account account){
        return new AccountEntity(account.id(),account.balance());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
