package dlemontagner.moneytransfer.infrastructure.adapters.spi.repositories;

import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.ports.spi.AccountsRepository;
import dlemontagner.moneytransfer.infrastructure.adapters.spi.repositories.entities.AccountEntity;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AccountsJpaAdapter implements AccountsRepository {
    private final AccountsJpaRepository accountsJpaRepository;

    public AccountsJpaAdapter(AccountsJpaRepository accountsJpaRepository) {
        this.accountsJpaRepository = accountsJpaRepository;
    }

    @Override
    public Account getById(UUID id) {
        return accountsJpaRepository.findById(id).get().toDomain();
    }

    @Override
    public Account save(Account account) {
        // Required for mockito's argThat to work
        if(account == null) return account;

        return accountsJpaRepository.save(AccountEntity.fromDomain(account)).toDomain();
    }
}
