package dlemontagner.moneytransfer.infrastructure.adapters.spi.repositories;

import dlemontagner.moneytransfer.infrastructure.adapters.spi.repositories.entities.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountsJpaRepository  extends JpaRepository<AccountEntity, UUID> {
}
