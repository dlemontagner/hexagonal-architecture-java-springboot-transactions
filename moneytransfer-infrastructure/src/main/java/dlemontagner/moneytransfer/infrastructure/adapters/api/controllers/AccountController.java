package dlemontagner.moneytransfer.infrastructure.adapters.api.controllers;

import dlemontagner.moneytransfer.domain.entities.Account;
import dlemontagner.moneytransfer.domain.ports.api.ManageAccountUseCase;
import dlemontagner.moneytransfer.infrastructure.requests.OpenAccountRequest;
import dlemontagner.moneytransfer.infrastructure.resources.AccountResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    private final ManageAccountUseCase accountManager;

    public AccountController(ManageAccountUseCase accountManager) {
        this.accountManager = accountManager;
    }

    @PostMapping
    public ResponseEntity<AccountResource> openAccount(@RequestBody OpenAccountRequest openAccountRequest){
        Account openedAccount = accountManager.openAccount(openAccountRequest.initialBalance());
        return created(fromMethodCall(on(this.getClass()).viewAccount(openedAccount.id())).build().toUri())
                .body(new AccountResource(openedAccount));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountResource> viewAccount(@PathVariable UUID id){
        return ok(new AccountResource(accountManager.viewAccount(id)));
    }

}
