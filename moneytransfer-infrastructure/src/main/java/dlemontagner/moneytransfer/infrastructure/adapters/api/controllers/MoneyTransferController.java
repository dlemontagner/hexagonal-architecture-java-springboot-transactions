package dlemontagner.moneytransfer.infrastructure.adapters.api.controllers;

import dlemontagner.moneytransfer.domain.exceptions.InsufficientBalanceException;
import dlemontagner.moneytransfer.domain.ports.api.ManageAccountUseCase;
import dlemontagner.moneytransfer.infrastructure.requests.TransferMoneyRequest;
import dlemontagner.moneytransfer.infrastructure.resources.TransferOutcomeResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/transfer")
public class MoneyTransferController {
    private final ManageAccountUseCase accountManager;

    public MoneyTransferController(ManageAccountUseCase accountManager) {
        this.accountManager = accountManager;
    }

    @PostMapping
    public ResponseEntity<TransferOutcomeResource> transferMoney(@RequestBody TransferMoneyRequest transferMoneyRequest){
        accountManager.transferMoney(transferMoneyRequest.source(), transferMoneyRequest.destination(), transferMoneyRequest.amount());
        return ok(new TransferOutcomeResource("success","Transfer completed successfully."));
    }

    @ExceptionHandler(InsufficientBalanceException.class)
    ProblemDetail handleInsufficientBalanceExceptions(InsufficientBalanceException e) {
        return ProblemDetail
                .forStatusAndDetail(HttpStatus.BAD_REQUEST ,e.getMessage());
    }

}
