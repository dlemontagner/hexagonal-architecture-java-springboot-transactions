package dlemontagner.moneytransfer.infrastructure.requests;

import java.math.BigDecimal;

public record OpenAccountRequest(BigDecimal initialBalance) {
}
