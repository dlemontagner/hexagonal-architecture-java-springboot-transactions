package dlemontagner.moneytransfer.infrastructure.requests;

import java.math.BigDecimal;
import java.util.UUID;

public record TransferMoneyRequest(UUID source, UUID destination, BigDecimal amount) {
}
