package dlemontagner.moneytransfer.infrastructure.resources;

public record TransferOutcomeResource(String status, String message) {
}
