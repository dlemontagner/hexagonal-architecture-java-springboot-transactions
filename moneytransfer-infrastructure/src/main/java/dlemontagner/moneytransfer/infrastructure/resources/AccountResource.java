package dlemontagner.moneytransfer.infrastructure.resources;

import dlemontagner.moneytransfer.domain.entities.Account;

import java.math.BigDecimal;
import java.util.UUID;

public record AccountResource(UUID id, BigDecimal balance) {
    public AccountResource(Account domainAccount){
        this(domainAccount.id(),domainAccount.balance());
    }
}
