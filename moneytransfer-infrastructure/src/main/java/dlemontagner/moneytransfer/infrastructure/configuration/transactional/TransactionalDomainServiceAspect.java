package dlemontagner.moneytransfer.infrastructure.configuration.transactional;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TransactionalDomainServiceAspect {
    private final TransactionalExecutor transactionalExecutor;

    public TransactionalDomainServiceAspect(TransactionalExecutor transactionalExecutor) {
        this.transactionalExecutor = transactionalExecutor;
    }

    @Pointcut("@within(ddd.DomainService)")
    private void withinDomainService() {

    }

    @Around("withinDomainService()")
    private Object domainService(ProceedingJoinPoint proceedingJoinPoint) {
        return transactionalExecutor.executeInTransaction(() -> {
            try {
                return proceedingJoinPoint.proceed();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        });
    }

}
