package dlemontagner.moneytransfer.infrastructure.configuration;

import ddd.DomainService;
import dlemontagner.moneytransfer.domain.MoneyTransferDomain;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;


@Configuration
@ComponentScan(
        basePackageClasses = {MoneyTransferDomain.class},
        includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {DomainService.class})}
)
public class DomainConfiguration {
}


